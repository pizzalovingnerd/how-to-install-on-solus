# How to install stuff on Solus

This is how to install tons of applications not on the Solus repo. You will not find anything on the Solus repo, unless it was a mistake

## To install a Flatpak

Go to the [flathub website](https://flathub.org), and read the setup insturctions. Then search for the app and install it.

## To install a Snap

Open a terminal and type:

```snap install APPLICATION```

or if your using Solus 4 or up, it's in the software center.

## To install a AppImage

Go to the appimage linked on this ReadMe file, and download it. Once it's downloaded, mark it executable and run it.

## Jar

This is just a Jar file you can run if you have Java installed.

## Installer

This is a installer that you just run. Kind of like Windows MSI files.

## Wine

If the application is a Windows app, you may need to use Wine to run it.

## Steam

To install something on Steam, download Steam, make an account, go to the Steam store, and buy games. Not every Steam game will be listed, only steam games that also have a Snap, Flatpak, or Appimage. [Here is a list of all steam games.](https://store.steampowered.com/search/?sort_by=Name_ASC&category1=998%2C994&os=linux) However Software on Steam is listed.

## Binary

If you can just download it and run a file, it will be marked as a Binary.

## Other method

If you can't install it with a Flatpak, Snap, Appimage, it will be listed as "Other Method" which will link to a tutorial on how to install it. Compiling from Source doesn't count as Other Method because you can pretty much compile anything from Source.

## Applications

0 A.D. (Flatpak, Snap)

7-Zip (Wine)

Adventure Editor (Flatpak, [Binary](https://github.com/bladecoder/bladecoder-adventure-engine/releases))

Addaps ([AppImage](https://www.addaps.com))

Agenda (Flatpak)

AIMP (Wine)

Alacritty (Snap (Unoffical))

Albion Online (Flatpak, Steam)

Aldiun ([AppImage](https://github.com/AlduinApp/alduin/releases))

Ampare Applications (Snap)

AnimationMaker (Snap)

Anatine ([Binary](https://github.com/sindresorhus/anatine/releases))

Anbox (Snap)

AnaVis ([AppImage](https://github.com/learningmedia/anavis/releases))

Anki (Flatpak, [Other Method](https://apps.ankiweb.net))

AnyDesk (Flatpak, [Binary](https://anydesk.com/platforms/linux))

Ao (Snap)

AppEditor (Flatpak)

AppGameKit (Steam)

AppImageUpdate ([AppImage](https://github.com/AppImage/AppImageUpdate/releases))

apktool (Snap)

Arduino (Snap, [Binary](https://www.arduino.cc/en/Main/Software))

Arizen ([AppImage](https://github.com/ZencashOfficial/arizen/releases))

Astroffers ([AppImage](https://github.com/jayhasyee/astroffers/releases))

AstroPrint ([AppImage](https://www.astroprint.com/products/p/astroprint-desktop))

asciinema (Snap)

Aseprite (Steam)

asunder-casept (Snap)

ARK (Flatpak, [Binary](https://github.com/ArkEcosystem/ark-desktop/releases))

atari800-jz (Snap)

Art of Illusion ([Jar, Installer](http://www.artofillusion.org/downloads#linux))

AtCore ([AppImage](https://download.kde.org/stable/atcore/))

Atomic Tanks (Flatpak)

Aten ([AppImage](https://www.projectaten.com/aten/download))

AutoTileGen (Steam)

Auryo (Snap, [AppImage](https://github.com/Superjo149/auryo/releases))

Authenticator (Flatpak)

AVI MetaEdit (Flatpak)

aws-cli (Snap)

Axeman (Snap)

Axis Game Factory (Steam)

AzPainter ([AppImage](https://github.com/Symbian9/azpainter/releases))

Battle of Wesnoth (Flatpak, Steam)

Battle Tanks (Flatpak)

BetaFlight Configurator (Flatpak)

Bijiben (Flatpak)

BlackOut II (Flatpak)

Books (Flatpak)

Bibisco ([Binary](http://www.bibisco.com/))

bitlbee (Snap)

BitTicker (Snap)

BitWarden (Snap, [AppImage](https://bitwarden.com/#download))

Bdash ([AppImage](https://www.bdash.io/))

Bezique ([AppImage](https://github.com/Sriep/Bezique/releases))

Black Chocobo ([AppImage](https://github.com/sithlord48/blackchocobo/releases))

BlackMirror ([AppImage](https://github.com/sorentycho/blackmirror/releases))

Blink1Control2 ([AppImage](https://github.com/todbot/Blink1Control2/releases))

BlueJ (Jar, Installer)

Bodhi ([AppImage](https://github.com/bodhiproject/bodhi-app/releases))

Bomber (Snap)

GNOME Boxes (Flatpak)

BrainVerse ([AppImage](https://github.com/ReproNim/brainverse/releases))

Briss (Snap, Jar)

Brook (Snap, Binary)

Buka (Snap, [AppImage](https://sourceforge.net/projects/buka/))

Bunq Desktop (Snap, [AppImage](https://github.com/BunqCommunity/BunqDesktop/releases/tag/0.8.6))

Buckets ([AppImage](https://github.com/buckets/application/releases))

Buka (Snap [AppImage](https://github.com/oguzhaninan/Buka))

Bussard (Snap)

BytespeicherTrayIcon ([AppImage](https://github.com/Bytespeicher/BytespeicherTrayIcon/releases))

BWF MetaEdit (Flatpak)

Calculist ([AppImage (Outdated)](https://github.com/calculist/calculist-desktop/releases))

Canoe ([AppImage, Binary](https://getcanoe.io/download/))

Cantana ([AppImage (Outdated)](https://bintray.com/unruhschuh/AppImages/Cantata/))

Caprine ([AppImage](https://sindresorhus.com/caprine/))

Cerebral-Debugger ([AppImage](https://github.com/cerebral/cerebral-debugger/releases))

Chatty ([Jar](http://chatty.github.io/#download))

ChessX (Flatpak)

Chromium (Snap, [AppImage (Outdated)](https://bintray.com/probono/AppImages/Chromium))

Chromium B.S.U. (Flatpak)

CIAA Suite ([AppImage](https://github.com/martinribelotta/embedded-ide-builder/releases))

Ciano (Flatpak)

Cleep Desktop([AppImage](https://github.com/tangb/CleepDesktop/releases))

CLion (Snap, [Installer](https://www.jetbrains.com/clion/download/#section=linux))

CoCoMusic ([AppImage, Binary](https://github.com/xtuJSer/CoCoMusic))

Code (Not to get confused with VSCode) (Flatpak)

Color Picker ([AppImage](https://github.com/Toinane/colorpicker/releases))

Cozy Desktop ([AppImage](https://github.com/cozy-labs/cozy-desktop/releases))

Cointop (Flatpak, Snap)

CopyQ (Flatpak)

Cow's Revenge (Flatpak, [Binary](https://pipoypipagames.itch.io/cows-revenge))

CPod (Also known as Cumulonimbus) (Snap, Flatpak, [AppImage](https://github.com/z-------------/cumulonimbus/releases))

cpustat (Snap)

Cromberg ([AppImage](https://github.com/z17/home-accounting-system/releases))

Cryplicity ([AppImage](https://github.com/Kthulu120/mangle/releases))

Crypter ([AppImage](https://github.com/HR/Crypter/releases))

CUBA Studio ([AppImage](https://www.cuba-platform.com/download))

CubicSDR ([AppImage](https://github.com/cjcliffe/CubicSDR/releases))

Cutegram ([Installer](http://aseman.co/en/products/cutegram/))

CutePeaks ([AppImage](https://github.com/labsquare/CutePeaks/releases))

Cutter ([AppImage](https://github.com/radareorg/cutter/releases))

CuteMarkEd (Flatpak)

Dana ([AppImage](https://github.com/b00f/dana/releases))

DataGrip (Snap)

D-Feet (Flathub)

Digital: A Love Story (Flathub)

Dino Client (Snap)

Dippi (Flathub)

Discographer (Snap)

DeDop Studio ([AppImage](https://github.com/DeDop/dedop-studio/releases))

Deepin Image Viewer (Snap)

Deepin Music (Snap)

Deepin Voice Recorder (Snap)

DeerPortal (Snap)

DevRantron (Snap)

Drakon (Snap)

Draw.io (Flatpak)

Drawpile ([AppImage](https://drawpile.net/download/#Linux))

Drawpile Server ([AppImage](https://drawpile.net/download/#Linux))

DV Analyzer (Flathub)

Dwarf Fortress (Snap)

Dwyco Phoo ([AppImage](https://github.com/blindchimp/dwyco/releases))

Electorrent ([AppImage](https://github.com/Tympanix/Electorrent/releases))

Electrum (Snap)

Elisa (Flatpak)

Ember ([AppImage](https://www.worldforge.org/index.php/components/ember/))

Engauge (Flatpak)

EnvKey ([AppImage](https://github.com/envkey/envkey-ui/releases))

Eqonomize ([AppImage](https://github.com/Eqonomize/Eqonomize/releases))

EyeStalker ([AppImage](https://github.com/tbrouns/eyestalker/releases))

Eolie (Flatpak)

Exodus (Flatpak)

Explode Bricks (Snap)

Extreme Tuxracer (Flatpak)

FastQt ([AppImage](https://github.com/labsquare/fastQt/releases))

Fedora Media Writer (Flatpak)

FeedTheMonkey ([Appimage](https://github.com/jeena/FeedTheMonkey/releases))

Filmulator ([AppImage](https://github.com/CarVac/filmulator-gui/releases))

Flatpak Developer Demo (Flatpak)

Flowblade (Flatpak)

Font Finder (Flatpak)

GNOME Font Viewer (Flatpak)

Foobar2000 (Wine, Snap)

Fractal (Flatpak)

Fragments (Flatpak)

Fraktal ([AppImage](https://github.com/Fraktal-JS/fraktal/releases))

Franz ([AppImage](https://github.com/meetfranz/franz/releases))

Freac ([Binary](https://www.freac.org/index.php/en/downloads-mainmenu-33))

FreeDM (Flatpak)

FreeBase ([AppImage](https://fontba.se/downloads/linux))

FreeDoom: Phase 1 (Flatpak)

FreeDoom: Phase 2 (Flatpak)

FreeCell Solitare (Snap)

FreeCad ([AppImage](https://www.freecadweb.org/wiki/Download))

FreeCraft (Snap)

Frozen Bubble (Flatpak)

FoxIt ([AppImage](https://www.foxitsoftware.com/downloads/))

Fugio (Snap)

Functy (Snap)

gbrainy (Flatpak)

GameCake (Snap)

Genius (Flatpak)

GeoGebra (Flatpak, [Binary](https://www.geogebra.org/download))

GetIt (Flatpak)

Ghost Desktop (Snap)

GifCurry ([AppImage](https://github.com/lettier/gifcurry/releases))

GitHoard ([AppImage}(https://github.com/jojobyte/githoard/releases))

GraphQL IDLE ([AppImage](https://github.com/jojobyte/githoard/releases))

Gitter (Flatpak, Snap)

GoLand (Snap)

Goxel (Flatpak, Snap)

Grafana (Snap)

Granatier (Snap)

Greensand (Snap)

Gravit Designer (Snap)

gsubs ([AppImage](https://github.com/sanjevirau/gsubs/releases))

Gydl (Flatpak)

Habitica (Snap)

HackUp (Flatpak)

Hashit (Snap)

Hatch Previewer (Flatpak)

Hexo Editor ([AppImage](https://github.com/zhuzhuyule/HexoEditor))

Hextris (Snap)

Hiri (Snap, [Binary](https://www.hiri.com/download_hiri/linux/))

hmtimer (Flatpak)

Hanoi-Towers (Snap)

HydraPaper (Flatpak)

HyperDEX ([AppImage](https://appimage.github.io/HyperDEX/))